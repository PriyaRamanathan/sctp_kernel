# README #

Build the CUSTOM LINUX KERNEL image for the support of OAI based MME Container migration.
Initial SCTP handshake process and SCTP abort/termination process are slightly modified to support the migration of SCTP based application.

### What is this repository for? ###

* SCTP state machine changes to support the SCTP pause and restore options in the Linux Kernel


### How do I get set up? ###

Use the custom kernel image which has the SCTP state machine modifications

### What services would be available?

A prototype to show the MME container migration successful scenario using CRIU.




